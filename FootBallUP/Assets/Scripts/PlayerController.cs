﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;


public class PlayerController : MonoBehaviour
{
    //Hello Abhay

    public  delegate void PlatformReached(int number);

    public static PlatformReached OnPlatformReached;




    [SerializeField]
    float speed = 5f;

    [SerializeField]
    GameObject StartPoint;

    [SerializeField]
    GameObject EndPoint;

    [SerializeField]
    GameObject Goalie;

    [SerializeField]
    GameObject ball;

    [SerializeField]
    LineRenderer lineRenderer;

    [SerializeField]
    CinemachineVirtualCamera cam;

    Vector3[] postions;

    [SerializeField]
    Animator[] PlayerAnimators;

    int platformLevel = 1;

    // Start is called before the first frame update
    void Start()
    {
        OnPlatformReached?.Invoke(platformLevel);
    }

    // Update is called once per frame
    void Update()
    {
        
        float horizontal = Input.GetAxis("Horizontal");
        Vector3 direction = new Vector3(0, 0, -horizontal);
        transform.Translate(direction * speed * Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ShootBall();
        }

    }

    private void ShootBall()
    {
        StartCoroutine(KickBall());
    }


    IEnumerator KickBall()
    {
        PlayerAnimators[platformLevel-1].SetTrigger("Kick");
        yield return new WaitForSeconds(0.5F);
        cam.Follow = ball.transform;
        postions = new Vector3[lineRenderer.positionCount];
        for (int i = 0; i < postions.Length; i++)
        {
            postions[i] = lineRenderer.GetPosition(i);
        }
        LeanTween.moveSpline(ball, postions, 1).setOnComplete(() => OnBallReached());
        
    }
    
    private void OnBallReached()
    {
        float height = transform.position.y;
        StartPoint.transform.position = EndPoint.transform.position;
        ball.transform.position = StartPoint.transform.position;
        EndPoint.transform.position = Goalie.transform.position;
        cam.Follow = StartPoint.transform;
        Vector3 offset = (EndPoint.transform.position - StartPoint.transform.position) / 2f;
        offset.y = height;
        transform.position = offset;
        platformLevel += 1;
        OnPlatformReached?.Invoke(platformLevel);
    }

    
    

}


