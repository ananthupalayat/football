﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    GameObject ball;

    Vector3 direction;

    [SerializeField]
    int platformLevel;

    Vector3 targetDirection;
    private void OnEnable()
    {
        PlayerController.OnPlatformReached += PlayerOnThisPlatform;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    void Tackle()
    {
        GetComponent<Animator>().SetTrigger("Tackle");
        LeanTween.move(gameObject, transform.position + transform.forward*13, 1f);
    }

    void PlayerOnThisPlatform(int playerLevel)
    {
        if (platformLevel == playerLevel)
        {
            //GetComponent<Animator>().SetTrigger("Run");
            
            targetDirection =transform.position-ball.transform.position;
            //LeanTween.rotateY(gameObject, ball.transform.position.y, 5);
            //LeanTween.move(gameObject, ball.transform.position - transform.forward * 10, 2).setOnComplete(() => Tackle());
        }
    }
}
