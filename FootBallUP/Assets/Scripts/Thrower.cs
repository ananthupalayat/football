﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thrower : MonoBehaviour
{
    [SerializeField]
    GameObject m_Ball;

    GameObject[] m_TrajectoryPoints;

    [SerializeField]
    GameObject m_TrajectoryPoint;

    [SerializeField]
    int m_NumOfPoints = 10;

    [SerializeField]
    float m_RotationSpeed = 30;

    [SerializeField]
    float m_PointScale = 1f;

    [SerializeField]
    float m_DistanceOffset = 1f;

    [SerializeField]
    float m_LaunchForce = 5f;

    float verticalAxis = default;
    float horizontalAxis = default;


    // Start is called before the first frame update
    void Start()
    {
        m_TrajectoryPoints = new GameObject[m_NumOfPoints];
        for (int i = 0; i < m_NumOfPoints; i++)
        {
            m_TrajectoryPoints[i] = Instantiate(m_TrajectoryPoint, transform.position, Quaternion.identity);
            m_TrajectoryPoints[i].transform.localScale *= m_PointScale;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        verticalAxis = Input.GetAxis("Vertical");
        horizontalAxis = Input.GetAxis("Horizontal");
        transform.Rotate(new Vector3(verticalAxis, horizontalAxis, 0) * Time.deltaTime*m_RotationSpeed);
        for(int i = 0; i < m_TrajectoryPoints.Length; i++)
        {
            m_TrajectoryPoints[i].transform.position = PointToPostion(i * m_DistanceOffset);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ShootBall();
        }
    }

    void ShootBall()
    {
        GameObject spawnedBall = Instantiate(m_Ball, transform.position, Quaternion.identity);
        spawnedBall.GetComponent<Rigidbody>().velocity = transform.forward * m_LaunchForce;
    }

    Vector3 PointToPostion(float t)
    {
        Vector3 currentPositon = transform.position + (transform.forward.normalized * m_LaunchForce* t) + 0.5f * Physics.gravity * (t * t);
        return currentPositon;
    }
}
